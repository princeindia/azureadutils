# azureadutils
Azure ad utility library contains several azure feature such as appinsights log, login to azure ad etc"


# Azure Login using redirect 

You just need to instanctiate AzureAdLogin with two parameters first parameter is MSAL login configuration and second one is prompt type we only support two prompt type. if is true then it will behave as sso otherwise login will always ask for password.
Read more about the authConfiguration here: 
https://docs.microsoft.com/en-us/azure/active-directory/develop/msal-js-initializing-client-applications#configuration-options

```
let azureAdLogonInstance = new AzureAdLogin(authConfiguration(), false);
let isLoggedIn = azureAdLogonInstance.login();

```

# Token expiration event listener

Your access token will expire based on the azure ad setting and you have fetch fresh access token to run application or call api etc. 
We provide 'AccessTokenExpired' event which application need to listen and fetch access token based on scope.

```
window.addEventListener('AccessTokenExpired', fetchAccessToken);

```
# Fetch Access Token

use azureadlogin class instance and call getAccessToken function and provide the access token scope which is string array.

```
azureAdLogonInstance.getAccessToken(accessTokenScope).then(authResp => {
             let token  = authResp.accessToken;
        })
        .catch(err => {
          console.log(err);
        })

```

# Log Out 

Just call logout function of azureAdLogin call instance.

```
azureAdLogonInstance.logout();

```

# Sample code for react application 

```
import React, {useEffect, useState, useCallback} from 'react';
import './App.css';
import {AzureAdLogin} from "azureadutils";
import { authConfiguration } from './constants';
 
function App() {
 
  const accessTokenScope = ['user.read'];
  const [logonInstance, setLogonInstance] = useState(null);
  const [isAccessToken, setAccessToken] = useState(null);
 
  const fetchAccessToken = useCallback(
    () => {
      if(logonInstance){
        logonInstance.getAccessToken(accessTokenScope).then(authResp => {
             setAccessToken(authResp.accessToken);
        })
        .catch(err => {
          console.log(err);
        })
     }
    }, [accessTokenScope]);
  
 
  useEffect(() => {
    console.log('Inside use effect empty');
    window.addEventListener('AccessTokenExpired', fetchAccessToken);
    return () => {
      window.removeEventListener('AccessTokenExpired', fetchAccessToken);
    }
  }, [fetchAccessToken])
 
  useEffect(() => {
    console.log('Inside use effect logonInstance', logonInstance);
    if(logonInstance){
      fetchAccessToken();
    }else{
      let azureAdLogonInstance = new AzureAdLogin(authConfiguration(), false);
      let isLoggedIn = azureAdLogonInstance.login();
      if(isLoggedIn){
        setLogonInstance(azureAdLogonInstance);
      }
    }
 
    return () => {
    }
  }, [logonInstance, fetchAccessToken])
 
  const logout = () => {
    if(logonInstance){
      logonInstance.logout();
    }
  }
 
  
 
  return (
    <div className="App">
       {
         isAccessToken ?  (<>We have received the access token <button onClick={() => {logout()}} >LogOut</button></>) : 
         logonInstance ? <button onClick={() => {logout()}} >LogOut</button> : 'Loading...'
       }
        
    </div>
  );
}
 
export default App;

```