import {
  ApplicationInsights,
  ITraceTelemetry,
  SeverityLevel,
  IExceptionTelemetry,
  IEventTelemetry,
  IPageViewTelemetry
} from '@microsoft/applicationinsights-web';
import { ICustomProperties } from '@microsoft/applicationinsights-core-js';

/**
 * The Singleton class defines the `getInstance` method that lets clients access
 * the unique singleton instance.
 */

export class AzureAppInsightLog {
  private instrumentationKey: string;
  private appInsights: ApplicationInsights;

  /**
   * The Singleton's constructor should always be private to prevent direct
   * construction calls with the `new` operator.
   */
  constructor(_instrumentationKey: string) {
    if (!_instrumentationKey) {
      throw new Error('AppInsight is required');
    }

    this.instrumentationKey = _instrumentationKey;
    this.appInsights = new ApplicationInsights({
      config: { instrumentationKey: this.instrumentationKey }
    });
    this.appInsights.loadAppInsights();
  }

  /**
   * Log information message
   */

  public info(message: any, customProperties: ICustomProperties): void {
    if (this.appInsights) {
      const _messageString = JSON.stringify(message);
      const trace: ITraceTelemetry = {
        message: _messageString,
        severityLevel: SeverityLevel.Information
      };

      this.appInsights.trackTrace(trace, customProperties);
      this.appInsights.flush();
    } else {
      console.error(
        'Please instanciate azure app insight log before using log'
      );
    }
  }

  /**
   * Log error message
   * @param message
   * @param name
   */

  public error(message: any, name: string): void {
    if (this.appInsights) {
      const _messageString = JSON.stringify(message);
      const _error: Error = {
        message: _messageString,
        name
      };
      const exception: IExceptionTelemetry = {
        error: _error,
        severityLevel: SeverityLevel.Error
      };

      this.appInsights.trackException(exception);
      this.appInsights.flush();
    } else {
      console.error(
        'Please instanciate azure app insight log before using log'
      );
    }
  }

  /**
   * Log custome event name
   * @param name
   * @param customProperties
   */

  public customEvent(name: string, customProperties: ICustomProperties): void {
    if (this.appInsights) {
      const _event: IEventTelemetry = {
        name
      };

      this.appInsights.trackEvent(_event, customProperties);
      this.appInsights.flush();
    } else {
      console.error(
        'Please instanciate azure app insight log before using log'
      );
    }
  }

  /**
   * Log page view
   * @param pagename
   * @param url
   */

  public pageview(pagename: string, url: string): void {
    if (this.appInsights) {
      const _pageview: IPageViewTelemetry = {
        name: pagename,
        uri: url
      };
      this.appInsights.trackPageView(_pageview);
      this.appInsights.flush();
    } else {
      console.error(
        'Please instanciate azure app insight log before using log'
      );
    }
  }
}
