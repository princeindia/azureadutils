import { UserAgentApplication, Configuration } from 'msal';
import { Utilities } from './Utilities/Utilities';

export class AzureAdLogin {
  private loginConfig: Configuration;
  private authProvider: UserAgentApplication;
  private interval: any;
  private isSSO: boolean = false;

  constructor(_loginConfig: Configuration, _isSSO: boolean) {
    this.loginConfig = _loginConfig;
    this.isSSO = _isSSO;
  }

  public login(): boolean {
    let isLoggedIn = false;
    if (this.loginConfig) {
      this.authProvider = new UserAgentApplication(this.loginConfig);
      this.authProvider.handleRedirectCallback(this.authCallback);

      if (this.authProvider.getAccount()) {
        isLoggedIn = true;
      } else {
        const authParams = {
          prompt: this.isSSO ? 'none' : 'login'
        };

        this.authProvider.loginRedirect(authParams);
      }
    } else {
      console.error('Login Configuration is required');
    }

    return isLoggedIn;
  }

  /**
   * Auth call back function is used only when we use login redirect and if there is any issue
   * in login redirect or response we log
   * @param error
   * @param response
   */

  private authCallback(error: any, response: any): void {
    //handle redirect response

    if (error) {
      console.error(`MSAL Auth Call Back: `, error);
    }

    if (response) {
      console.log(`MSAL Auth Call Back: `, response);
    }
  }

  /**
   *  log out function will logout the current user
   */

  public logout(): void {
    if (this.authProvider) {
      this.authProvider.logout();
    } else {
      console.error('No auth provider exists');
    }
  }

  /**
   * Get access token function is used to fetch access token based on scope provided
   * @param _scopes
   */

  public getAccessToken(_scopes: Array<string>): Promise<any> {
    const accessTokenPromise = new Promise((resolve, reject) => {
      if (this.authProvider) {
        if (Array.isArray(_scopes) && _scopes.length > 0) {
          this.authProvider
            .acquireTokenSilent({ scopes: _scopes })
            .then(authResponse => {
              const intervalTime = 60 * 1000;
              this.interval = Utilities.setTokenRefreshListner(
                authResponse.accessToken,
                intervalTime,
                this.interval
              );
              resolve(authResponse);
            })
            .catch(err => {
              console.error(err);
              if (
                err.name === 'InteractionRequiredAuthError' ||
                err.name === 'ClientAuthError'
              ) {
                console.log('Aquireing token using redirect');
                this.authProvider.acquireTokenRedirect({ scopes: _scopes });
              } else {
                reject(err);
              }
            });
        } else {
          console.error('Invalid token scope provided');
          reject('Invalid token scope provided');
        }
      } else {
        console.error('No auth provider exists');
        reject('No auth provider exists');
      }
    });

    return accessTokenPromise;
  }
}
