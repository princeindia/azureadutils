export class Utilities {
  public static isTokenRefreshRequired(
    intervalTime: number,
    token: string
  ): boolean {
    let isRefreshRequired = false;

    const parsedToken = this.decodeJwtToken(token);
    if (parsedToken !== null) {
      const issuedTime = this.getNestedObject(parsedToken, ['iat']);
      const expirationTime = this.getNestedObject(parsedToken, ['exp']);
      const currentTime = new Date().valueOf() / 1000;
      console.log(
        `Issued Time:: ${issuedTime} Expiration Time:: ${expirationTime} Current Time:: ${currentTime}`
      );

      if (expirationTime <= currentTime + intervalTime / 1000) {
        isRefreshRequired = true;
      }
    }

    return isRefreshRequired;
  }

  private static decodeJwtToken(token: string): any {
    let response = null;
    try {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const jsonPayload = decodeURIComponent(
        atob(base64)
          .split('')
          .map(function(c): string {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join('')
      );

      response = JSON.parse(jsonPayload);
    } catch (exception) {
      //
    }
    return response;
  }

  private static getNestedObject(nestedObj: any, pathArr: Array<string>): any {
    if (
      nestedObj !== null &&
      nestedObj !== undefined &&
      pathArr !== null &&
      pathArr !== undefined &&
      Array.isArray(pathArr)
    ) {
      return pathArr.reduce(
        (obj, key) =>
          obj && obj[key] !== undefined && obj[key] !== null ? obj[key] : '',
        nestedObj
      );
    }
  }

  public static setTokenRefreshListner(
    token: string,
    intervalTime: number,
    interval: any
  ): any {
    clearInterval(interval);
    interval = setInterval(() => {
      if (Utilities.isTokenRefreshRequired(intervalTime, token)) {
        console.log('AccessTokenExpired event triggered');
        const tokenExpiredEvent = new Event('AccessTokenExpired');
        window.dispatchEvent(tokenExpiredEvent);
      }
    }, intervalTime);

    return interval;
  }
}
